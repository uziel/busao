/**
 * Javascript for Default Theme
 */


$(document).ready(function() {
    
    //buscar linhas
    $("#enviar1").click(function() {
        $('#resultado ul').html("");
        var label = "";
        var links = "";
        $.ajax({
            dataType: "jsonp", 
            url: Croogo.basePath + 'linhas/getLinhas/' + $('#de').val(),
            success: function(data){
                $.each(data, function(i,item){
                    label = "";
                    links = "";
                    label = '<div class="descricao">' + item.Linha.descricao + '</div>';
                    links = '<div class="options">' +
                                '<a href="#" linhaId = "'+ item.Linha.id +'" class="ver-horarios">Horários</a>' +
                                '<a href="#" linhaId = "'+ item.Linha.id +'" class="ver-vias">Vias</a>' +
                            '</div>';
                    $('#resultado ul').append('<li> ' + label + links +  ' </li>');
                });
            }   
        });
        return false;
    });
    
    $(".ver-vias").live("click", function(){
        $('#resultado ul').html("");
        var label = "";
        var links = "";
        $.ajax({
            dataType: "jsonp", 
            url: Croogo.basePath + 'linhas/getViasByLinha/' + $(this).attr('linhaId'),
            success: function(data){
                $.each(data, function(i,item){
                    label = "";
                    links = "";
                    if(item.Via.sentido == 'I') {
                        label += '<div class="sentido"><img src="'+Croogo.basePath +'/img/ida.png"/></div>';
                    } else {
                        label += '<div class="sentido"><img src="'+Croogo.basePath +'/img/volta.png"/></div>';
                    }
                    
                    label += '<div class="descricao">' + item.Via.descricao + '</div>';
                    
                    $('#resultado ul').append('<li> ' + label  +  ' </li>');
                });
            }   
        });
        return false;
    });
    
    $(".ver-horarios").live("click", function(){
        $('#resultado ul').html("");
        var label = "";
        var links = "";
        $.ajax({
            dataType: "jsonp", 
            url: Croogo.basePath + 'linhas/getHorariosByLinha/' + $(this).attr('linhaId'),
            success: function(data){
                $.each(data, function(i,item){
                    label = "";
                    links = "";
                    
                    label += '<div class="turno">' + item.Horario.turno + '</div>';
                    label += '<div class="tipo-horario">' + item.Horario.tipo_id + '</div>';
                    label += '<div class="descricao">' + item.Horario.hora + '</div>';
                    links = '<div class="options">' +
                                '<a href="#" viaId = "'+ item.Horario.id +'" class="ver-horarios">Horários</a>' +
                                '<a href="#" viaId = "'+ item.Horario.id +'" class="ver-vias">Vias</a>' +
                            '</div>';
                    $('#resultado ul').append('<li> ' + label + links +  ' </li>');
                });
            }   
        });
        return false;
    });
    
    //jquery ui
    $('.tabs').tabs();
    $("button").button();
});