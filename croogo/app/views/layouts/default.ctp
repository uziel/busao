<?php
/**
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title_for_layout; ?></title>
        <link rel="stylesheet" type="text/css" href="/busao/croogo/css/theme.css" media="screen, projection"/>
	<?php
		echo $this->Html->css(array(
                    //'theme',
                    '/ui-themes/smoothness/jquery-ui.css',
                    
		));
		echo $this->Layout->js();
		echo $this->Html->script(array(
			'jquery/jquery.min',
			'jquery/jquery-ui.min',
			'theme',
		));
		echo $scripts_for_layout;
	?>
</head>
    <body>
        <div id="wrapper">
            <div id="header" >
                <div class="logo">
                    <?php echo $html->image('logo.png');?>
                </div>
            </div>

            <div id="main">
                <?php
                    echo $this->Layout->sessionFlash();
                    echo $content_for_layout;
                ?>
            </div>

            <div id="footer">

            </div>
        </div>
    </body>
</html>