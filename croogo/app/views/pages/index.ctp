<div class="pages index">
    
    <div class="tabs">
        <ul>
                <li><a href="#tab-1"><span>De / Para</span></a></li>
                <li><a href="#tab-2"><span>Linha</span></a></li>
        </ul>

        <div id="tab-1">
            <div class="fields">
                <ul>
                    <li>
                        <label>De:</label>
                        <input type="text" name="de" id="de"/>
                    </li>
                    <li>
                        <label>Para:</label>
                        <input type="text" name="para" id="para"/>
                    </li>
                </ul>
            </div>
            <div class="buttons">
                 <button id="enviar1">Buscar</button>
            </div>
            
            <div id="resultado">
                <ul>
                    
                </ul>
            </div>
            
        </div>
        
        <div id="tab-2">
            <div class="fields">
                <ul>
                    <li>
                        <label>Linha:</label>
                        <input type="text" name="linha" id="linha"/>
                    </li>
                </ul>
            </div>
            <div class="buttons">
                 <button onclick="buscarLinhas();" id="enviar1">Buscar</button>
            </div>
        </div>

</div>