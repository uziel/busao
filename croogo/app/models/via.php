<?php
/**
 * Via
 *
 * PHP version 5
 *
 * @category Model
 * @package  Croogo
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Via extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'Via';
        
        public $belongsTo = array(
		'Linha' => array(
			'className' => 'Linha',
			'foreignKey' => 'linha_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
	);


}
