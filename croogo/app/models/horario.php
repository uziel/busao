<?php
/**
 * Horario
 *
 * PHP version 5
 *
 * @category Model
 * @package  Croogo
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Horario extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'Horario';
        
        public $belongsTo = array(
		'Linha' => array(
			'className' => 'Linha',
			'foreignKey' => 'linha_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
                'Tipo' => array(
			'className' => 'Tipo',
			'foreignKey' => 'tipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
		),
	);


}
