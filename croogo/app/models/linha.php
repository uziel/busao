<?php
/**
 * Linha
 *
 * PHP version 5
 *
 * @category Model
 * @package  Croogo
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class Linha extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
	public $name = 'Linha';
        
        public $hasMany = array(
		'Via' => array(
			'className' => 'Via',
			'foreignKey' => 'linha_id',
			'dependent' => true,
		),
                'Horario' => array(
			'className' => 'Horario',
			'foreignKey' => 'linha_id',
			'dependent' => true,
		),
	);

}
