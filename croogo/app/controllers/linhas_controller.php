<?php
/**
 * Linhas Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Croogo
 * @version  1.0
 * @author   Uziel Barbosa <contato@uzielbarbosa.com.br>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class LinhasController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Linhas';

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Linha', 'Via', 'Horario');
        
        var $components = array('RequestHandler');
       

	public function getLinhas($termo = null) {
            
            Configure::write ('debug', 0);
            
            $condicoes = array();
            if($termo) {
                $condicoes['Linha.descricao LIKE'] = '%' . $termo . '%';
            }
            
            $dados = $this->Linha->find('all', array('conditions' => $condicoes));
            
            $this->layout = 'json/default';
            
            $result = json_encode($dados);
            $this->set(compact('result'));
            $this->set('callback', $this->params['url']['callback']);
            
        }
        
        public function getViasByLinha($linhaId = null) {
            
            $condicoes = array('Via.linha_id' => $linhaId);
            
            $dados = $this->Via->find('all', array('conditions' => $condicoes));
            
            $this->layout = 'json/default';
            
            $result = json_encode($dados);
            
            
            $this->set(compact('result'));
            $this->set('callback', $this->params['url']['callback']);
            Configure::write ('debug', 0);
        }
        
        public function getHorariosByLinha($linhaId = null) {
            
            $condicoes = array('Horario.linha_id' => $linhaId);
            
            $dados = $this->Horario->find('all', array('conditions' => $condicoes));
            
            $this->layout = 'json/default';
            
            $result = json_encode($dados);
            
            $this->set(compact('result'));
            $this->set('callback', $this->params['url']['callback']);
            Configure::write ('debug', 0);
        }
        
        public function getLinkMapaByLinha($linhaId = null) {
            $retorno = array();
            $sentidos = array();
            $sentidos[0] = 'I';
            $sentidos[1] = 'V';
            $condicoes = array('Mapa.linha_id' => $linhaId);
            $dados = $this->Mapa->find('all', array('conditions' => $condicoes));
            if (count($dados) > 0){
                foreach ($dados as $value) {
                    if($value->linkRotaIda!=NULL){
                        $retorno[0]=$value->linkRotaIda;
                    }
                    if($value->linkRotaVolta!=NULL){
                        $retorno[1]=$value->linkRotaVolta;
                    }
                }
            }
            if(count($retorno)!=2){
                for ($index = 0; $index < count($sentidos); $index++) {
                    $condicoes = array('Via.linha_id' => $linhaId, 'Via.sentido' => $sentidos[$index]);
                    $dados = $this->Via->find('all', array('conditions' => $condicoes));
                    $urlmaps = "maps.google.com.br/maps?saddr=";
                    $ter = 0;
                    foreach ($dados as $dados2) {
                        $tex = str_replace(" ", "+", $dados2);
                        if ($ter == 0) {
                            $urlmaps.="$tex";
                        } elseif ($ter == 1) {
                            $urlmaps.="&daddr=$tex";
                        } else {
                            $urlmaps.="+to:$tex";
                        }
                        $urlmaps.=",+Macei&oacute;+-+AL";
                        $ter++;
                    }
                    $urlmaps.="&hl=pt-BR";
                    if(!isset($retorno[$sentidos[$index]]))
                        $retorno[$sentidos[$index]] = $urlmaps;
                }
            }

            $this->layout = 'json/default';

            $result = json_encode($retorno);

            $this->set(compact('result'));
            $this->set('callback', $this->params['url']['callback']);
            Configure::write('debug', 0);
        }
        

}
